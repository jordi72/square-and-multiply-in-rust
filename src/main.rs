use std::env;
use std::error::Error;
use std::time::Instant;

fn main() {
    let now = Instant::now();
    let args: Vec<String> = env::args().collect();
    match handle_commands(&args) {
        Ok(result) => {
            // Print green
            println!("\x1b[32mResult: {}\x1b[0m", result);
        }
        Err(e) => {
            // print red
            eprintln!("\x1b[31mError: {}\x1b[0m", e);
        }
    }
    let elapsed = now.elapsed();
    println!("elapsed time: {:?} micro seconds", elapsed.as_micros());
}
fn square_and_multiply(base: i32, exponential: Vec<u8>, modulo: i32) -> i32 {
    //if exp[i] == 0 result * result % modulo
    //if exp[i] == 1 result * result % modulo && result * base % modulo
    let mut result: i32 = 1;
    for i in exponential.iter() {
        match i % 2 {
            0 => result *= result % modulo,
            _ => result = (result * result % modulo) * base % modulo
        }
    }
    result
}
fn to_binary(mut n: i32) -> Vec<u8> {
    //a trick to know how many digits 'n' is going to be in binary.
    let s: usize = ((n as f64).log2().floor() + 1.0) as usize; //about 300 nanos

    //making the vector of the precise size required for the binary representation of 'n'
    //this will be more efficent 
    let mut v: Vec<u8> = vec![0;s]; //most inneficient part, init time increases with s size.

    for i in v.iter_mut() {
        if n < 1 { break };
        match n % 2 {
            0 => *i = 0,
            _ => *i = 1 
        }
        n /= 2;
    }

    v.into_iter().rev().collect()
}
fn handle_commands(args: &[String]) -> Result<i32, Box<dyn Error>> {
    match args.len() {
        7 => {
            let b: &String = &args[1];
            let e: &String = &args[3];
            let m: &String = &args[5];
            if b == "-b" && e == "-e" && m == "-m" {
                let bn: i32 = args[2].parse()?;
                let en: i32 = args[4].parse()?;
                let mn: i32 = args[6].parse()?;

                let en_in_binary = to_binary(en);
                let result = square_and_multiply(bn, en_in_binary, mn);
                Ok(result)
            } else {
                Err("run `cargo run -- -b number -e number -m number` example: `cargo run -- -b 23 -e 373 -m 747`".into())
            }
        }
        _ => {
            Err("run `cargo run -- -b number -e number -m number` example: `cargo run -- -b 23 -e 373 -m 747`".into())
        }
    }
}